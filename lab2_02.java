public class lab2_02 {  

    private static void printArray2_2(int[] nums,int count){
        for(int i=0;i<nums.length-count;i++){
            System.out.print(nums[i]+" ");
        }
        for(int j=nums.length-count;j<nums.length;j++){
            System.out.print("_"+" ");
        }
        System.out.println();
    }

    
    public static void main(String[] args) {
        int[] nums = {0,1,2,2,3,0,4,2};      
        int val = 2;
        int countVal = 0;
        int countNotVal =0;
        int leftshift = 0;

        for(int i=0;i<nums.length;i++){
            if(nums[i]!=val){
               countNotVal++; 
            }
        }

        for(int i=0;i<nums.length;i++){
            if(nums[i]==val){
               countVal++; 
            }
        }


        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[leftshift] = nums[i];
                leftshift++;
            }
        }

        System.out.print(countNotVal+", nums = ");
        printArray2_2(nums, countVal);

    }
}
